﻿using Assets.Scripts.Configuration;
using UnityEngine;

namespace Assets.Scripts.Player
{
    public class Snowplow
    {
        private readonly float _maxAcceleration;
        private readonly float _plowSpeedMultiplier;

        public Vector2 Velocity { get; private set; }
        public bool PlowDown { get; private set; }
        public UpgradeableStats Stats { get; private set; }

        public float Score { get; private set; }
        private float _money;
        public float Money
        {
            get { return _money; }
            set
            {
                if (value > _money)
                {
                    var addition = value - _money;
                    Score += addition;
                }
                _money = value;
            }
        }

        public float SnowLevel { get; set; }
        public float CurrentSnowSpeedMultiplier
        {
            get
            {
                if (SnowLevel == 0)
                {
                    return 1;
                }
                var multiplier = 1 / (SnowLevel / Stats.EffectivePlowLevel);
                return multiplier > 1 ? 1 : multiplier;
            }
        }

        public float CurrentPlowSpeedMultiplier
        {
            get { return PlowDown ? _plowSpeedMultiplier : 1; }
        }

        public Snowplow(SnowplowConfig config)
        {
            _maxAcceleration = config.MaxAcceleration;
            _plowSpeedMultiplier = config.PlowSpeedMultiplier;
            PlowDown = config.PlowDown;
            Stats = new UpgradeableStats(config.UpgradeableStatsConfig);
        }

        public void TogglePlow()
        {
            PlowDown = !PlowDown;
        }

        public void Accelerate(float throttlePercentageX, float throttlePercentageY)
        {
            var targetVelocityX = throttlePercentageX * Stats.MaxVelocity * CurrentPlowSpeedMultiplier * CurrentSnowSpeedMultiplier;
            var targetVelocityY = throttlePercentageY * Stats.MaxVelocity * CurrentPlowSpeedMultiplier * CurrentSnowSpeedMultiplier;
            var targetVelocity = new Vector2(targetVelocityX, targetVelocityY);
            Velocity = Vector2.MoveTowards(Velocity, targetVelocity, _maxAcceleration);
        }
    }
}
