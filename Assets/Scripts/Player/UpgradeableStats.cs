﻿using Assets.Scripts.Configuration;

namespace Assets.Scripts.Player
{
    public class UpgradeableStats
    {
        private readonly float _tireSpeedMultiplier;
        private readonly int _plowLevelMultiplier;

        public UpgradeableStats(UpgradeableStatsConfig config)
        {
            _tireSpeedMultiplier = config.TireSpeedMultiplier;
            TireLevelPoints = config.TireLevel;
            _plowLevelMultiplier = config.PlowLevelMultiplier;
            PlowLevelPoints = config.PlowLevel;
        }

        public float MaxVelocity
        {
            get { return TireLevelPoints * _tireSpeedMultiplier; }
        }

        public int TireLevelPoints { get; set; }
        public int PlowLevelPoints { get; set; }
        public int EffectivePlowLevel { get { return PlowLevelPoints * _plowLevelMultiplier; } }
    }
}