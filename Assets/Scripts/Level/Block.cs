﻿using Assets.Scripts.Configuration;
using UnityEngine;

namespace Assets.Scripts.Level
{
    public class Block
    {
        public float DamagePerTick { get; private set; }
        public float DamageThreshold { get; private set; }
        public float Health { get; private set; }
        public float MaxHealth { get; private set; }

        public float HealthPercent { get { return Health / MaxHealth; } }

        public bool IsAlive { get { return Health > 0; } }

        public Block(BlockConfig config)
        {
            DamagePerTick = config.DamagePerTick;
            DamageThreshold = config.DamageThreshold;
            Health = config.InitialHealth;
            MaxHealth = config.MaxHealth;
        }

        public void DoDamage(float snowAmount)
        {
            if (!IsAlive)
            {
                return;
            }

            if (snowAmount > DamageThreshold)
            {
                Health = Mathf.Max(Health - DamagePerTick, 0);
            }
            else
            {
                Health = Mathf.Min(Health + (DamagePerTick * 2), MaxHealth);
            }
        }

        public Color GetHealthColor()
        {
            return new Color(1, HealthPercent, HealthPercent);
        }
    }
}
