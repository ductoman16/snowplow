﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Configuration;
using Assets.Scripts.Level.Tiles;

namespace Assets.Scripts.Level
{
    public class City
    {
        public Block[,] Blocks { get; private set; }

        public ITile[,] Tiles { get; private set; }

        public City(CityConfig config)
        {
            PopulateBlocks(config);

            var sizeX = config.BlocksX * config.TilesPerBlockX - 1;
            var sizeY = config.BlocksY * config.TilesPerBlockY - 1;

            Tiles = CreateTiles(config, sizeX, sizeY);
        }

        private void PopulateBlocks(CityConfig config)
        {
            Blocks = new Block[config.BlocksX, config.BlocksY];
            var blockConfig = config.BlockConfig;
            Blocks.ForEach((x, y) =>
            {
                Blocks[x, y] = new Block(blockConfig);
            });
        }

        private ITile[,] CreateTiles(CityConfig config, int sizeX, int sizeY)
        {
            var tiles = new ITile[sizeX, sizeY];
            tiles.ForEach((x, y) =>
            {
                tiles[x, y] = CreateTile(config, x, y);
            });
            return tiles;
        }

        private ITile CreateTile(CityConfig config, int x, int y)
        {
            if (x % config.TilesPerBlockX == config.TilesPerBlockX - 1 ||
                y % config.TilesPerBlockY == config.TilesPerBlockY - 1)
            {
                return new RoadTile();
            }
            var blockX = x / config.TilesPerBlockX;
            var blockY = y / config.TilesPerBlockY;
            return new BuildingTile(Blocks[blockX, blockY]);
        }

        public float GetSnowAmount(Block block)
        {
            var adjacentRoadTiles = GetAdjacentRoadTiles(block);
            return adjacentRoadTiles.Sum(r => r.SnowLevel);
        }

        private IEnumerable<RoadTile> GetAdjacentRoadTiles(Block block)
        {
            var adjacentRoadTiles = new List<RoadTile>();

            Tiles.ForEach((x, y, tile) =>
            {
                var buildingTile = tile as BuildingTile;
                if (buildingTile == null)
                {
                    return;
                }

                if (buildingTile.Block != block)
                {
                    return;
                }

                var roadTiles = GetAdjacentRoadTiles(x, y);
                adjacentRoadTiles.AddRange(roadTiles);
            });
            return adjacentRoadTiles;
        }

        private IEnumerable<RoadTile> GetAdjacentRoadTiles(int x, int y)
        {
            var ret = new List<RoadTile>(); //TODO: WTF linq was breaking this so I had to hack this in.

            var tile1 = SafeGetTile(x - 1, y - 1);
            if (tile1 is RoadTile)
            {
                ret.Add(tile1 as RoadTile);
            }
            var tile2 = SafeGetTile(x, y - 1);
            if (tile2 is RoadTile)
            {
                ret.Add(tile2 as RoadTile);
            }
            var tile3 = SafeGetTile(x + 1, y - 1);
            if (tile3 is RoadTile)
            {
                ret.Add(tile3 as RoadTile);
            }
            var tile4 = SafeGetTile(x - 1, y);
            if (tile4 is RoadTile)
            {
                ret.Add(tile4 as RoadTile);
            }
            var tile5 = SafeGetTile(x, y);
            if (tile5 is RoadTile)
            {
                ret.Add(tile5 as RoadTile);
            }
            var tile6 = SafeGetTile(x + 1, y);
            if (tile6 is RoadTile)
            {
                ret.Add(tile6 as RoadTile);
            }
            var tile7 = SafeGetTile(x - 1, y + 1);
            if (tile7 is RoadTile)
            {
                ret.Add(tile7 as RoadTile);
            }
            var tile8 = SafeGetTile(x, y + 1);
            if (tile8 is RoadTile)
            {
                ret.Add(tile8 as RoadTile);
            }
            var tile9 = SafeGetTile(x + 1, y + 1);
            if (tile9 is RoadTile)
            {
                ret.Add(tile9 as RoadTile);
            }

            return ret;
        }

        public ITile SafeGetTile(int x, int y)
        {
            try
            {
                return Tiles[x, y];
            }
            catch (IndexOutOfRangeException)
            {
                return null;
            }
        }
    }
}
