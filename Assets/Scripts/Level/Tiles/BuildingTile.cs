﻿namespace Assets.Scripts.Level.Tiles
{
    public class BuildingTile : ITile
    {
        public Block Block { get; private set; }

        public BuildingTile(Block block)
        {
            Block = block;
        }
    }
}
