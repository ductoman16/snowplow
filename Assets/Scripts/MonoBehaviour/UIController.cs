﻿using System.Text;
using Assets.Scripts.Level;
using Assets.Scripts.Player;
using Assets.Scripts.Upgrades;
using ModestTree;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Zenject;

namespace Assets.Scripts.MonoBehaviour
{
    public class UIController : UnityEngine.MonoBehaviour
    {
        public Text MoneyDisplay;
        public Text BlockHealthContainer;
        public Text SnowLevelDisplay;
        public Text ErrorDisplay;
        public float ErrorTimeSeconds = 3;
        private float _errorTimeRemaining;

        public Text[] ScoreDisplay;

        public Button[] RestartButtons;
        public Button[] QuitButtons;
        public Button ResumeButton;

        public Text TireLevelDisplay;
        public Button TireUpgradeButton;

        public Text PlowLevelDisplay;
        public Button PlowUpgradeButton;

        private Snowplow _snowplow;
        private City _city;
        private Store _store;
        private GameStateManager _gameStateManager;

        [Inject]
        public void Inject(Snowplow snowplow, City city, Store store, GameStateManager gameStateManager)
        {
            _snowplow = snowplow;
            _city = city;
            _store = store;
            _gameStateManager = gameStateManager;
        }

        // Use this for initialization
        void Start()
        {
            TireUpgradeButton.onClick.AddListener(TryUpgradeTires);

            PlowUpgradeButton.onClick.AddListener(TryUpgradePlow);

            RestartButtons.ForEach(b => b.onClick.AddListener(RestartGame));
            QuitButtons.ForEach(b => b.onClick.AddListener(QuitGame));
            ResumeButton.onClick.AddListener(ResumeGame);
        }

        private void TryUpgradePlow()
        {
            try
            {
                _store.UpgradePlow(_snowplow);
            }
            catch (NotEnoughMoneyException)
            {
                DisplayNotEnoughMoneyMessage();
            }
        }

        private void TryUpgradeTires()
        {
            try
            {
                _store.UpgradeTires(_snowplow);
            }
            catch (NotEnoughMoneyException)
            {
                DisplayNotEnoughMoneyMessage();
            }
        }

        private void ResumeGame()
        {
            _gameStateManager.GameState = GameState.Active;
        }

        private void QuitGame()
        {
            SceneManager.LoadScene("Menu");
        }

        private void RestartGame()
        {
            SceneManager.LoadScene("Main");
        }

        private void DisplayNotEnoughMoneyMessage()
        {
            ErrorDisplay.text = "Not enough money!";
            _errorTimeRemaining = ErrorTimeSeconds;
        }

        // Update is called once per frame
        void Update()
        {
            MoneyDisplay.text = _snowplow.Money.ToString("C");
            ScoreDisplay.ForEach(s => s.text = _snowplow.Score.ToString("F"));

            BlockHealthDisplay();

            SnowLevelDisplay.text = ((int)_snowplow.SnowLevel).ToString();

            TireUpgradeButton.GetComponentInChildren<Text>().text = _store.TireUpgradeCost.ToString("C");
            PlowUpgradeButton.GetComponentInChildren<Text>().text = _store.PlowUpgradeCost.ToString("C");

            TireLevelDisplay.text = _snowplow.Stats.TireLevelPoints.ToString();
            PlowLevelDisplay.text = _snowplow.Stats.PlowLevelPoints.ToString();

            if (_errorTimeRemaining <= 0)
            {
                ErrorDisplay.text = "";
            }
            else
            {
                _errorTimeRemaining -= Time.deltaTime;
            }

            if (Input.GetKeyUp(KeyCode.JoystickButton0))
            {
                TryUpgradeTires();
            }
            if (Input.GetKeyUp(KeyCode.JoystickButton1))
            {
                TryUpgradePlow();
            }
        }

        private void BlockHealthDisplay()
        {
            var sb = new StringBuilder("Block Health:\r\n");
            _city.Blocks.ForEach((x, y, block) =>
            {
                sb.AppendFormat("Block {0},{1} : {2}%\r\n", x, y, block.HealthPercent * 100);
            });
            BlockHealthContainer.text = sb.ToString();
        }
    }
}
