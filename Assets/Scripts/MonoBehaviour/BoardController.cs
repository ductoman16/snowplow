﻿using System.Linq;
using Assets.Scripts.Level;
using Assets.Scripts.Level.Tiles;
using Assets.Scripts.Player;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using Random = UnityEngine.Random;

namespace Assets.Scripts.MonoBehaviour
{
    public class BoardController : UnityEngine.MonoBehaviour
    {
        public int BlockSizeX;
        public int BlockSizeY;

        public float MoneyPerSnowMultiplier = .01f;

        public GameObject RoadTileHorizontal;
        public GameObject RoadTileVertical;
        public GameObject SnowTile;
        public GameObject BuildingTile;
        public Sprite[] BuildingSprites;
        public SnowplowController SnowplowController;
        public GameObject MiniMapCanvas;
        public GameObject MiniMapPanel;
        public Sprite WarningIcon;
        public Sprite DeadIcon;

        private GameObject[,] _miniMap;

        public GameObject Board;

        private City _city;
        private Snowplow _snowplow;
        private GameStateManager _gameStateManager;

        [Inject]
        public void Inject(City city, Snowplow snowplow, GameStateManager gameStateManager)
        {
            _city = city;
            _snowplow = snowplow;
            _gameStateManager = gameStateManager;
        }

        void Start()
        {
            _city.Tiles.ForEach(CreateTileGameObject);
            RandomlyPlaceSnowplow();
            CreateMiniMap();
        }

        private void CreateMiniMap()
        {
            _miniMap = new GameObject[_city.Blocks.GetLength(0), _city.Blocks.GetLength(1)];
            _city.Blocks.ForEach((x, y) =>
            {
                var panel = Instantiate(MiniMapPanel);
                panel.transform.SetParent(MiniMapCanvas.transform);
                _miniMap[x, y] = panel;
            });
        }

        private void RandomlyPlaceSnowplow()
        {
            var snowTiles = Board.GetComponentsInChildren<SnowTileController>();

            var randomSnowTile = snowTiles[Random.Range(0, snowTiles.Length)];
            SnowplowController.GetComponent<Transform>().position = randomSnowTile.GetComponent<Transform>().position;
        }

        private void CreateTileGameObject(int x, int y, ITile tile)
        {
            GameObject tileGameObject;
            var roadTile = tile as RoadTile;
            if (roadTile != null)
            {
                tileGameObject = CreateRoadTileGameObject(x, y, roadTile);
            }
            else
            {
                tileGameObject = CreateBuildingTileGameObject(tile as BuildingTile);
            }

            var tileTransform = tileGameObject.GetComponent<Transform>();
            tileTransform.SetParent(Board.GetComponent<Transform>());
            tileTransform.localPosition = new Vector2(x, -y);
        }

        private GameObject CreateBuildingTileGameObject(BuildingTile buildingTile)
        {
            var randomBuildingSprite = BuildingSprites[Random.Range(0, BuildingSprites.Length)];

            var tileGameObject = Instantiate(BuildingTile);
            tileGameObject.GetComponent<SpriteRenderer>().sprite = randomBuildingSprite;
            tileGameObject.GetComponent<BuildingTileController>().Tile = buildingTile;
            return tileGameObject;
        }

        private GameObject CreateRoadTileGameObject(int x, int y, RoadTile roadTile)
        {
            var roadTilePrefab = HasRoadTilesHoriztonallyAdjacent(x, y)
                ? RoadTileHorizontal
                : RoadTileVertical;
            var tileGameObject = Instantiate(roadTilePrefab);
            var snowGameObject = Instantiate(SnowTile);
            snowGameObject.GetComponent<Transform>().SetParent(tileGameObject.GetComponent<Transform>());
            var snowTileController = snowGameObject.GetComponent<SnowTileController>();
            snowTileController.RoadTile = roadTile;
            snowTileController.OnSnowPlowed += OnSnowPlowed;
            return tileGameObject;
        }

        private bool HasRoadTilesHoriztonallyAdjacent(int x, int y)
        {
            var left = _city.SafeGetTile(x - 1, y);
            var right = _city.SafeGetTile(x + 1, y);
            return left is RoadTile || right is RoadTile;
        }

        private void OnSnowPlowed(float amountPlowed)
        {
            //TODO: The board controller shouldn't be responsible for distributing money
            _snowplow.Money += MoneyPerSnowMultiplier * amountPlowed;
        }

        void FixedUpdate()
        {
            foreach (var block in _city.Blocks)
            {
                var snowAmount = _city.GetSnowAmount(block);
                block.DoDamage(snowAmount);
            }

            foreach (var cityBlock in _city.Blocks)
            {
                if (!cityBlock.IsAlive)
                {
                    _gameStateManager.GameState = GameState.Lost;
                }
            }

            ClampToBoundaries();
        }

        void Update()
        {
            ColorMiniMap();
        }

        private void ColorMiniMap()
        {
            _miniMap.ForEach((x, y, miniMapPanel) =>
            {
                var cityBlock = _city.Blocks[y, x];
                var healthColor = cityBlock.GetHealthColor(); //TODO: This is y,x because the board is rendered the wrong way
                var image = miniMapPanel.GetComponent<Image>();

                image.color = healthColor;
                if (cityBlock.HealthPercent <= 0)
                {
                    image.sprite = DeadIcon;
                }
                else if (cityBlock.HealthPercent < .25)
                {
                    image.sprite = WarningIcon;
                }
                else
                {
                    image.sprite = null;
                }
            });
        }

        private void ClampToBoundaries()
        {
            var transforms = Board.GetComponentsInChildren<Transform>();
            var minX = transforms.Min(t => t.position.x);
            var maxX = transforms.Max(t => t.position.x);
            var minY = transforms.Min(t => t.position.y);
            var maxY = transforms.Max(t => t.position.y);

            var newX = Mathf.Clamp(SnowplowController.transform.position.x, minX, maxX);
            var newY = Mathf.Clamp(SnowplowController.transform.position.y, minY, maxY);
            SnowplowController.transform.position = new Vector2(newX, newY);
        }
    }
}
