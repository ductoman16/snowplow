﻿using UnityEngine;
using Zenject;

namespace Assets.Scripts.MonoBehaviour
{
    public class GameStateController: UnityEngine.MonoBehaviour
    {
        public GameObject PauseMenu;
        public GameObject GameOverMenu;

        private GameStateManager _gameStateManager;

        [Inject]
        public void Inject(GameStateManager gameStateManager)
        {
            _gameStateManager = gameStateManager;
        }

        void Start()
        {
            _gameStateManager.GameState = GameState.Active;
        }

        void Update()
        {
            if (_gameStateManager.GameState == GameState.Paused)
            {
                Time.timeScale = 0;
                PauseMenu.SetActive(true);
            }
            else if (_gameStateManager.GameState == GameState.Lost)
            {
                GameOverMenu.SetActive(true);
                Time.timeScale = 0;
            }
            else
            {
                Time.timeScale = 1;
                PauseMenu.SetActive(false);
            }
        }
    }
}
