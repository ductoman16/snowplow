﻿using Assets.Scripts.Player;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.MonoBehaviour
{
    [RequireComponent(typeof(Rigidbody2D))]
    [RequireComponent(typeof(BoxCollider2D))]
    public class SnowplowController : UnityEngine.MonoBehaviour
    {
        public float VelocityMultiplier = 1;
        public float SnowSlowMultiplier = 1f;

        private Snowplow _snowplow;

        [Inject]
        public void Inject(Snowplow snowplow)
        {
            _snowplow = snowplow;
        }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        void FixedUpdate()
        {
            UpdateVelocity();
            UpdateSpriteRotation();
        }

        private void UpdateSpriteRotation()
        {
            var sprite = GetComponentInChildren<SpriteRenderer>();
            var velocity = GetComponent<Rigidbody2D>().velocity;
            if (Mathf.Abs(velocity.x) > Mathf.Abs(velocity.y))
            {
                if (velocity.x > 0)
                {
                    sprite.transform.rotation = Quaternion.Euler(0, 0, -90);
                }
                else if (velocity.x < 0)
                {
                    sprite.transform.rotation = Quaternion.Euler(0, 0, 90);
                }
            }
            else
            {
                if (velocity.y > 0)
                {
                    sprite.transform.rotation = Quaternion.Euler(0, 0, 0);
                }
                else if (velocity.y < 0)
                {
                    sprite.transform.rotation = Quaternion.Euler(180, 0, 0);
                }
            }
        }

        private void UpdateVelocity()
        {
            var targetVelocity = _snowplow.Velocity * VelocityMultiplier;
            GetComponent<Rigidbody2D>().velocity = targetVelocity;
        }

        public void HitSnow(float amount)
        {
            _snowplow.SnowLevel = amount;
        }
    }
}
