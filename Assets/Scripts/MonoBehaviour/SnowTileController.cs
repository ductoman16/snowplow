﻿using Assets.Scripts.Level.Tiles;
using UnityEngine;

namespace Assets.Scripts.MonoBehaviour
{
    [RequireComponent(typeof(SpriteRenderer))]
    public class SnowTileController : UnityEngine.MonoBehaviour
    {
        public int SnowMaxOpacity = 100;

        public RoadTile RoadTile { get; set; }

        public delegate void SnowPlowedEventHandler(float amountPlowed);
        public event SnowPlowedEventHandler OnSnowPlowed;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            UpdateSnowOpacity();
        }

        private void UpdateSnowOpacity()
        {
            var spriteRenderer = GetComponent<SpriteRenderer>();
            var color = spriteRenderer.color;
            var alpha = RoadTile.SnowLevel / SnowMaxOpacity;
            spriteRenderer.color = new Color(color.r, color.g, color.b, alpha);
        }

        void OnTriggerEnter2D(Collider2D collider)
        {
            var snowplowController = collider.GetComponent<SnowplowController>();
            if (snowplowController != null) 
            {
                snowplowController.HitSnow(RoadTile.SnowLevel);
            }
        }

        void OnTriggerExit2D(Collider2D collider)
        {
            var snowplowController = collider.GetComponent<SnowplowController>();
            if (snowplowController != null)
            {
                PlowSnow();
            }
        }

        private void PlowSnow()
        {
            var snowlevel = RoadTile.SnowLevel;
            RoadTile.SnowLevel = 0;
            FireOnSnowPlowed(snowlevel);
        }

        protected virtual void FireOnSnowPlowed(float amount)
        {
            if (OnSnowPlowed != null)
            {
                OnSnowPlowed(amount);
            }
        }
    }
}
