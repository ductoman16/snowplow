﻿using Assets.Scripts.Level;
using Assets.Scripts.Level.Tiles;
using UnityEngine;

namespace Assets.Scripts.MonoBehaviour
{
    [RequireComponent(typeof(SpriteRenderer))]
    public class BuildingTileController : UnityEngine.MonoBehaviour
    {
        public BuildingTile Tile { get; set; }

        public void Update()
        {
            UpdateBuildingHealthColor(Tile);
        }

        private void UpdateBuildingHealthColor(BuildingTile tile)
        {
            var spriteRenderer = GetComponent<SpriteRenderer>();
            spriteRenderer.color = tile.Block.GetHealthColor();
        }
    }
}
