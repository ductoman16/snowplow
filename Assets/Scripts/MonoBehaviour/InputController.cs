﻿using Assets.Scripts.Player;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.MonoBehaviour
{
    public class InputController : UnityEngine.MonoBehaviour
    {
        private Snowplow _snowplow;
        private GameStateManager _gameStateManager;

        [Inject]
        public void Inject(Snowplow snowplow, GameStateManager gameStateManager)
        {
            _snowplow = snowplow;
            _gameStateManager = gameStateManager;
        }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            var horizontal = Input.GetAxis("Horizontal");
            var vertical = Input.GetAxis("Vertical");

            _snowplow.Accelerate(horizontal, vertical);

            if (Input.GetKeyUp(KeyCode.Escape))
            {
                _gameStateManager.TogglePaused();
            }
        }
    }
}
