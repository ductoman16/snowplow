﻿using System.Linq;
using Assets.Scripts.Level;
using Assets.Scripts.Level.Tiles;
using Zenject;

namespace Assets.Scripts.MonoBehaviour
{
    public class SnowfallController : UnityEngine.MonoBehaviour
    {
        public float SnowfallRate = .09f;
        public float SnowfallDelta = .02f;
        public int SnowfallIncreasePeriod = 20;

        private float period;
        private City _city;

        [Inject]
        public void Inject(City city)
        {
            _city = city;
        }

        void Update()
        {
            if (period > SnowfallIncreasePeriod)
            {
                SnowfallRate += SnowfallDelta;
                period = 0;
            }
            period += UnityEngine.Time.deltaTime;
        }

        void FixedUpdate()
        {
            DoSnowfall();
        }

        private void DoSnowfall()
        {
            foreach (var roadTile in _city.Tiles.OfType<RoadTile>())
            {
                roadTile.SnowLevel += SnowfallRate;
            }
        }
    }
}
