﻿using Assets.Scripts.Configuration;
using Assets.Scripts.Player;

namespace Assets.Scripts.Upgrades
{
    public class Store
    {
        public int TireUpgradeCost { get; private set; }
        public int PlowUpgradeCost { get; private set; }

        private readonly int _tireUpgradeCostMultiplier;
        private readonly int _plowUpgradeCostMultiplier;

        public Store(StoreConfig config)
        {
            TireUpgradeCost = config.TireUpgradeInitialCost;
            PlowUpgradeCost = config.PlowUpgradeInitialCost;

            _tireUpgradeCostMultiplier = config.TireUpgradeCostMultiplier;
            _plowUpgradeCostMultiplier = config.PlowUpgradeCostMultiplier;
        }

        public void UpgradeTires(Snowplow snowplow)
        {
            if (snowplow.Money < TireUpgradeCost)
            {
                throw new NotEnoughMoneyException();
            }

            snowplow.Money -= TireUpgradeCost;
            snowplow.Stats.TireLevelPoints += 1;

            TireUpgradeCost = TireUpgradeCost * _tireUpgradeCostMultiplier;
        }

        public void UpgradePlow(Snowplow snowplow)
        {
            if (snowplow.Money < PlowUpgradeCost)
            {
                throw new NotEnoughMoneyException();
            }

            snowplow.Money -= PlowUpgradeCost;
            snowplow.Stats.PlowLevelPoints += 1;

            PlowUpgradeCost = PlowUpgradeCost * _plowUpgradeCostMultiplier;
        }
    }
}
