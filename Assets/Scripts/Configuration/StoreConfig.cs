using System;

namespace Assets.Scripts.Configuration
{
    [Serializable]
    public class StoreConfig
    {
        public int TireUpgradeInitialCost = 10;
        public int PlowUpgradeInitialCost = 10;

        public int TireUpgradeCostMultiplier = 2;
        public int PlowUpgradeCostMultiplier = 2;
    }
}