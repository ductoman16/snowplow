﻿using System;

namespace Assets.Scripts.Configuration
{
    [Serializable]
    public class CityConfig
    {
        public int BlocksX = 4;
        public int BlocksY = 4;

        public int TilesPerBlockX = 4;
        public int TilesPerBlockY = 4;

        public BlockConfig BlockConfig;
    }
}