﻿using System;

namespace Assets.Scripts.Configuration
{
    [Serializable]
    public class BlockConfig
    {
        public float MaxHealth = 15;
        public float InitialHealth = 15;
        public float DamagePerTick = 0.005f;
        public float DamageThreshold = 500;
    }
}