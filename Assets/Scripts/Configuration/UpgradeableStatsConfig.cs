﻿using System;

namespace Assets.Scripts.Configuration
{
    [Serializable]
    public class UpgradeableStatsConfig
    {
        public float TireSpeedMultiplier = 1.3f;
        public int TireLevel = 1;

        public int PlowLevelMultiplier = 200;
        public int PlowLevel = 1;
    }
}