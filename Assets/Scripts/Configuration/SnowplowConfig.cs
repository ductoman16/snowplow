﻿using System;

namespace Assets.Scripts.Configuration
{
    [Serializable]
    public class SnowplowConfig
    {
        public int MaxAcceleration = 5;
        public bool PlowDown = true;
        public float PlowSpeedMultiplier = .75f;
        public UpgradeableStatsConfig UpgradeableStatsConfig;
    }
}