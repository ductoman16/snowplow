﻿namespace Assets.Scripts
{
    public interface IInput
    {
        float Horizontal { get; }
        float Vertical { get; }
    }
}
