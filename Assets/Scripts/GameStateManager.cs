﻿namespace Assets.Scripts
{
    public class GameStateManager
    {
        public GameState GameState { get; set; }

        public void TogglePaused()
        {
            GameState = GameState == GameState.Active
                ? GameState.Paused
                : GameState.Active;
        }
    }

    public enum GameState
    {
        Active,
        Paused,
        Lost
    }
}
