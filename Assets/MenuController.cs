﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets
{
    public class MenuController : MonoBehaviour
    {
        public Button StartButton;
        public Button QuitButton;

        // Use this for initialization
        void Start()
        {
            StartButton.onClick.AddListener(() =>
            {
                SceneManager.LoadScene("Main");
            });
            QuitButton.onClick.AddListener(Application.Quit);
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyUp(KeyCode.JoystickButton0))
            {
                SceneManager.LoadScene("Main");
            }
        }
    }
}
