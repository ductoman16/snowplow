﻿using System;

namespace Assets
{
    public static class ArrayExtensions
    {
        public static void ForEach<T>(this T[,] array, Action<int, int, T> action)
        {
            for (int x = 0; x < array.GetLength(0); x++)
            {
                for (int y = 0; y < array.GetLength(1); y++)
                {
                    action(x, y, array[x, y]);
                }
            }
        }

        public static void ForEach<T>(this T[,] array, Action<int, int> action)
        {
            for (int x = 0; x < array.GetLength(0); x++)
            {
                for (int y = 0; y < array.GetLength(1); y++)
                {
                    action(x, y);
                }
            }
        }
    }
}
