﻿using Assets.Scripts;
using Assets.Scripts.Configuration;
using Assets.Scripts.Level;
using Assets.Scripts.Player;
using Assets.Scripts.Upgrades;
using Zenject;

namespace Assets
{
    public class ZenjectInstaller : MonoInstaller<ZenjectInstaller>
    {
        public SnowplowConfig SnowplowConfig;
        public CityConfig CityConfig;
        public StoreConfig StoreConfig;

        public override void InstallBindings()
        {
            Container.Bind<Snowplow>().AsSingle().WithArguments(SnowplowConfig);
            Container.Bind<City>().AsSingle().WithArguments(CityConfig);
            Container.Bind<Store>().AsSingle().WithArguments(StoreConfig);
            Container.Bind<GameStateManager>().AsSingle();
        }
    }
}
